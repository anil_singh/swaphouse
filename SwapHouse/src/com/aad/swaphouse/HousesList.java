package com.aad.swaphouse;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class HousesList extends Activity {

	ListView houseListView;
	List<Houses> house_ArrayList;
	CustomAdapter customAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.house_list);
		
		ParseObject.registerSubclass(Houses.class);
		Parse.initialize(this, "eNdYyZr6YIn42Y1STqmYp0Oku4miJRaClxIP4EKt", "gSvDdFeuMLMUP7f2ouJiWJFPnccUCc7ds0yrvRHe");
		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
		defaultACL.setPublicReadAccess(true);

		ParseACL.setDefaultACL(defaultACL, true);
		
		houseListView = (ListView) findViewById(R.id.houseList);
		String city = this.getIntent().getStringExtra("CityName");
		getDatafromDb(city);
	}

	private void getDatafromDb(String city) {
		ParseQuery<Houses> query = ParseQuery.getQuery("Houses");
		query.whereContains("city", city);
		query.findInBackground(new FindCallback<Houses>() {
			public void done(List<Houses> house_list, ParseException e) {
				if (e == null) {
					onSucessful(house_list);
				} else {
					Log.d("List", "Error "+e.toString());
				}
			}

		});
	}

	private void onSucessful(List<Houses> house_list) {
		house_ArrayList = house_list;
		for (int i = 0; i < house_list.size(); i++) {
			Log.d("List", "Size of List " + house_list.get(i).getCity());
		}
		if (house_ArrayList.size() > 0) {
			customAdapter = new CustomAdapter(this, house_ArrayList);
			houseListView.setAdapter(customAdapter);
		}
	}
}
