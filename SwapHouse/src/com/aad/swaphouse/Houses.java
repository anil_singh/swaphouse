package com.aad.swaphouse;

import java.util.Date;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

@ParseClassName("Houses")
public class Houses extends ParseObject {
	
	public Houses(){
		
	}
	public String getCity() {
		return getString("city");
	}

	public void setCity(String city) {
		put("city", city);
	}
	
	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		put("name", name);
	}

	public String getState() {
		return getString("state");
	}

	public void setState(String state) {
		put("state", state);
	}

	public ParseFile getPhotoFile() {
		return getParseFile("picture");
	}

	public void setPhotoFile(ParseFile file) {
		put("picture", file);
	}

	
	public String getPhoneNum() {
		return getString("phone");
	}

	public void setPhoneNum(String phone) {
		put("mobile", phone);
	}
	
	public String getDuration() {
		return getString("duration");
	}

	public void setDuration(String rent_duration) {
		put("duration", rent_duration);
	}

	public String getAddress() {
		return getString("houseNo");
	}

	public void setAddress(String address) {
		put("houseNo", address);
	}
	
	public String getPinCode() {
		return getString("pincode");
	}

	public void setPinCode(String pincode) {
		put("pincode", pincode);
	}
	
	public String getDescription() {
		return getString("description");
	}

	public void setDescription(String description) {
		put("description", description);
	}
	
	public Date getAvailabilty() {
		return getDate("available");
	}

	public void setAvailabilty(Date date) {
		put("available", date);
	}
	
	public void setSpaceType(String space_type) {
		put("type", space_type);
	}
	
	public String getSpaceType() {
		return getString("type");
	}
	
	
	public String getRooms() {
		return getString("space");
	}

	public void setSpace(String no_room) {
		put("space", no_room);
	}
	
}
