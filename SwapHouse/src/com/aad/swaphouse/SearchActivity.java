package com.aad.swaphouse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SearchActivity extends Activity {
	EditText editText;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		editText = (EditText) findViewById(R.id.location);
	}

	public void search(View view) {
		Intent intent = new Intent(this, HousesList.class);
		String city = editText.getText().toString();
		if (city.isEmpty()) {
			Toast.makeText(this, "Enter city name", Toast.LENGTH_LONG).show();
		}
		intent.putExtra("CityName", city);
		startActivity(intent);
	}
}
