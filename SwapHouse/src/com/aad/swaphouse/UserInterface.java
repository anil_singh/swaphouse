package com.aad.swaphouse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class UserInterface extends ActionBarActivity {

	EditText nameText, addressText, cityText, mobileText;
	EditText stateText, pincodeText, house_descriptionText;
	RadioButton house_type_RadioButton;
	Spinner room_Spinner;
	SeekBar duration_Bar;
	int no_room = 0;
	int rent_duration = 0;
	Date date;
	ImageView imageView;
	TextView dateTextView, seekBarTextView;
	Button addImg;
	private final int DATE_PICKER_ID = 1111;
	private int year;
	private int month;
	private int day;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		
		ParseObject.registerSubclass(Houses.class);
		Parse.initialize(this, "eNdYyZr6YIn42Y1STqmYp0Oku4miJRaClxIP4EKt", "gSvDdFeuMLMUP7f2ouJiWJFPnccUCc7ds0yrvRHe");
		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
		defaultACL.setPublicReadAccess(true);

		ParseACL.setDefaultACL(defaultACL, true);
		
		
		nameText = (EditText) findViewById(R.id.name_text);
		addressText = (EditText) findViewById(R.id.address_text);
		cityText = (EditText) findViewById(R.id.city_text);
		mobileText = (EditText) findViewById(R.id.mobile_text);
		stateText = (EditText) findViewById(R.id.state_text);
		pincodeText = (EditText) findViewById(R.id.pincode_text);
		house_descriptionText = (EditText) findViewById(R.id.house_desText);
		duration_Bar = (SeekBar) findViewById(R.id.seekBar);
		seekBarTextView = (TextView) findViewById(R.id.seekMonthViewer);
		dateTextView = (TextView) findViewById(R.id.date_text);
		room_Spinner = (Spinner) findViewById(R.id.swapSpinner);
		addImg = (Button) findViewById(R.id.profilePicButton);
		imageView = (ImageView) findViewById(R.id.profilePic);
		onRadioBtnClicked();
		onseekBarChange();
		onDateChange();
		onSpinerItemSelected();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_interface, menu);
		return true;
	}

	public void onRadioBtnClicked() {
		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.house_type);
		int selectedId = radioGroup.getCheckedRadioButtonId();
		house_type_RadioButton = (RadioButton) findViewById(selectedId);
	}

	public void onSpinerItemSelected() {
		room_Spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				no_room = pos + 1;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				Toast.makeText(UserInterface.this, "Select space for swap",
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void onDateChange() {
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		// Show current date

		StringBuilder stringBuilder = new StringBuilder().append(day)
				.append("-").append(month + 1).append("-").append(year)
				.append(" ");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
		try {
			date = dateFormat.parse(stringBuilder.toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		dateTextView.setText(stringBuilder);

		// Button listener to show date picker dialog

		Button changeDate = (Button) findViewById(R.id.selectDate);
		changeDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// On button click show datepicker dialog
				showDialog(DATE_PICKER_ID);

			}

		});
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:

			// open datepicker dialog.
			// set date picker for current date
			// add pickerListener listner to date picker
			return new DatePickerDialog(this, pickerListener, year, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;
			StringBuilder stringBuilder = new StringBuilder().append(day)
					.append("-").append(month + 1).append("-").append(year)
					.append(" ");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
			try {
				date = dateFormat.parse(stringBuilder.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Show selected date
			dateTextView.setText(stringBuilder);

		}
	};
	private String mCurrentPhotoPath;
	private String path_img;

	public void onseekBarChange() {

		duration_Bar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {

			}

			@Override
			public void onProgressChanged(SeekBar arg0, int progressValue,
					boolean arg2) {
				rent_duration = progressValue;
				seekBarTextView.setText(String.valueOf(rent_duration)
						+ " Months");
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			submitData();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void submitData() {
		Houses houseData = new Houses();
		houseData.setAddress(addressText.getText().toString());
		houseData.setCity(cityText.getText().toString());
		houseData.setState(stateText.getText().toString());
		houseData.setDuration(String.valueOf(rent_duration));
		houseData.setName(nameText.getText().toString());
		String phn = mobileText.getText().toString();
		houseData
				.setPhoneNum(phn);
		houseData
				.setPinCode(pincodeText.getText().toString());
		houseData.setSpace(String.valueOf(no_room));
		houseData.setPhotoFile(getByteImg());
		houseData.setDescription(house_descriptionText.getText().toString());
		onRadioBtnClicked();
		houseData.setSpaceType(house_type_RadioButton.getText().toString());
		houseData.setAvailabilty(date);
		
		houseData.saveInBackground(new SaveCallback() {
			
			@Override
			public void done(com.parse.ParseException e) {
				if (e == null) {
					UserInterface.this.setResult(Activity.RESULT_OK);
					Toast.makeText(
							UserInterface.this,
							"saving: ",
							Toast.LENGTH_SHORT).show();
					onSuccessful();
				} else {
					Toast.makeText(
							UserInterface.this,
							"Error saving: " + e.getMessage(),
							Toast.LENGTH_SHORT).show();
				}
				
			}
		});

			

	}

	protected void onSuccessful() {
startActivity(new Intent(this, SearchActivity.class));		
	}

	public ParseFile getByteImg() {
		Bitmap bitmap = BitmapFactory.decodeFile(path_img);
		Bitmap house_scaledImg = Bitmap.createScaledBitmap(bitmap, 200, 200
				* bitmap.getHeight() / bitmap.getWidth(), false);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		house_scaledImg.compress(Bitmap.CompressFormat.JPEG, 100, bos);
		byte[] scaledData = bos.toByteArray();
		ParseFile photoFile = new ParseFile("meal_photo.jpg", scaledData);
		return photoFile;

	}

	public void onSaveImg(Bitmap bitmap) {
		OutputStream output;
		File filepath = Environment.getExternalStorageDirectory();
		File dir = new File(filepath.getAbsolutePath()
				+ "/grabhouse/");
		dir.mkdirs();
		File file = new File(dir, "swaphouse.png");
		path_img = file.getAbsolutePath();
		Toast.makeText(UserInterface.this, path_img,
				Toast.LENGTH_LONG).show();
		try {

			output = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
			output.flush();
			output.close();
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onAddImage(View dummyview) {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(takePictureIntent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		// TODO Auto-generated method stub
		if (requestCode == 1 && resultCode == RESULT_OK && intent != null) {
			Bundle extras = intent.getExtras();
			Bitmap bitMap = (Bitmap) extras.get("data");
			imageView.setImageBitmap(bitMap);
			onSaveImg(bitMap);
		}
	}
}
