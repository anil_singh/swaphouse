package com.aad.swaphouse;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;

public class CustomAdapter extends BaseAdapter {

	List<Houses> houseList;
	Context context;

	public CustomAdapter(Context context, List<Houses> house_ArrayList) {
		this.context = context;
		this.houseList = house_ArrayList;
	}

	@Override
	public int getCount() {
		return houseList.size();
	}

	@Override
	public Object getItem(int count) {
		return null;
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		View row = view;
		ViewHolder holder;

		if (row == null) {
			@SuppressWarnings("static-access")
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.house, parent, false);
			holder = new ViewHolder(row);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		String address = houseList.get(postion).getAddress() + "\n"
				+ houseList.get(postion).getCity() + ", "
				+ houseList.get(postion).getState();
		holder.house_detail.setText(address);
		
		ParseFile photoFile = houseList.get(postion).getParseFile("picture");
		Log.v("LOG!!!!!!", "imageView height = "+photoFile.getName());
		if (photoFile != null) {
			holder.house_img.setParseFile(photoFile);
			holder.house_img.loadInBackground(new GetDataCallback() {

				@Override
				public void done(byte[] arg0, ParseException arg1) {
					                 
				     Log.v("LOG!!!!!!", "imageView height = ");      // DISPLAYS 90 px

				}
			});
		}
		
		return row;
	}

	public class ViewHolder {
		TextView house_detail;
		ParseImageView house_img;

		public ViewHolder(View view) {
			house_img = (ParseImageView) view.findViewById(R.id.houe_img);
			house_detail = (TextView) view.findViewById(R.id.house_description);
		}
	}
}
